<?php

include "init.php";

if(auth()){
    $user = auth();
}
include "partials/header.php";

?>
    <div class="mt-5 component-section bg-dark">
        <!--title-->
        <div class="text-center pb-md-5 pb-4">
            <?php if(auth()):?>

            <h6 class="mb-0 text-white">کاربر محترم <?= $user->username ?> به سایت ما خوش آمدید</h6>

            <?php else:?>

            <h6 class="mb-0 text-white">لطفا برای دیدن محتوای این برگه وارد سایت شوید کاربر محترم</h6>


            <?php endif;?>
        </div>
        <div class="container">
            <div class="row demo">
                <div class="col-md-12 mx-auto">

                    <div class="mb-4 mx-auto text-center">

                        <?php if(auth()):?>

                            <form action="<?=DO_LOGOUT ?>" method="post">
                                <div class="form-group">
                                    <button type="submit" class="btn btn-solid-light">خروج از سایت</button>
                                </div>

                            </form>

                        <?php else:?>

                            <a href="<?=LOGIN?>" class="btn btn-solid-light">ورود</a>
                            <a href="<?=REGISTER?>" class="btn btn-theme">ثبت نام</a>

                        <?php endif;?>


                    </div>

                </div>
            </div>
        </div>
    </div>

<?php


include "partials/footer.php";

?>