<?php

include "init.php";


include "partials/header.php";

?>

<div class="section-gap">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card border-0 row no-gutters flex-column flex-md-row">
                    <div class="card-body d-flex align-items-center col-lg-5 p-md-5 p-3">
                        <div class="w-100">
                            <form action="<?=DO_REGISTER?>" method="post">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="نام">
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="آدرس ایمیل">
                                </div>
                                <div class="form-group">
                                    <div class="icon-field-right">
                                        <i class="fa fa-eye"></i>
                                        <input type="password" name="password"  class="form-control" placeholder="رمز عبور">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-theme">ثبت نام</button>
                                </div>
                                <div class="form-group mt-lg-5">
                                    <a href="<?=LOGIN?>" class="">ورود</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="flex-column col-lg-7">
                        <div class="position-relative">
                            <img class="card-img-right flex-grow-1 " src="assets/img/cards/29a.jpg" alt="">
                            <div class="login-content">
                                <h2 class="">ثبت نام</h2>
                                <p>برای ایجاد حساب کاربری فرم را تکمیل نمایید</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php


include "partials/footer.php";

?>