<?php

const BASE_URL      = 'http://localhost/php3Tamrin/login-register/';
const BASE_PATH     = 'C:\\xampp\\htdocs\\php3Tamrin\\login-register\\';
const LOGIN         = BASE_URL.'login.php';
const REGISTER      = BASE_URL.'register.php';
const DO_LOGIN      = BASE_URL.'process/do_login.php';
const DO_LOGOUT     = BASE_URL.'process/do_logout.php';
const DO_REGISTER   = BASE_URL.'process/do_register.php';
const USER_DB       = BASE_PATH.'db\\users.json';

include 'functions.php';