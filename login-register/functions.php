<?php


//return stdClass or assocArray Posts
function getUsers($return_assoc = 0)
{

    $posts = json_decode(file_get_contents(USER_DB),$return_assoc);
    return array_reverse((array)$posts);
}


function getUser($email){

    $users = getUsers();

    foreach ($users as $user){

        if($user->email == $email){

            return $user;

        }
    }
    return false;

}

// $user->{name,email,password}
function saveUser(stdClass $user):bool
{
    $users    = getUsers(1);
    $users[]  = (array)$user;
    $users_js = json_encode($users);
    file_put_contents(USER_DB,$users_js);
    return true;

}


function login($email,$password){

    $users = getUsers();

    foreach ($users as $user){

        if($user->password == $password && $user->email == $email){

            return true;

        }
    }
    return false;
}


function logout(){


    if(isset($_COOKIE["user"])){

        setcookie('user',$_POST['email'], time() - (86400 * 30));

    };

    //setcookie("username", "", time()-3600);

}

function auth(){

    if(isset($_COOKIE["user"])){

        return getUser($_COOKIE["user"]);

    }else{

        return false;
    }

}